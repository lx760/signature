package com.aliyun.www.sync;

import com.alibaba.fastjson.JSON;
import com.google.common.io.BaseEncoding;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kyle
 * @date 2018/08/17
 */
public class Main {

    private static final String LINKEDMALL_PUBLICK_KEY ="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmB2d4NasdZEQ//lg7/h7ZFwuiyBn86a9SoE0gfquIkdFmEv2+8dj7AwxlsXidzxI4Ta9zkiZFHqgC3bmtlRuF6BgtS1+ubs7ksd3YG+kyk+H6dAb6LnhGf7rv7PTUxSb8WN8ytZbl/5li2NYJva2igiWhOQ9VITPFobYcbZLiaaRfRRUmkPGgbuP2ScgrKQJB6cy34/wpc0bYMoqLETTCKctZRnfX1G1d1E8meCKdWWHmQqsRFkA8+OxzBKMeKhrJYT3fa2lDdA9yQDQsWj+jbmMd42NE6VnOQWpI/afsCNalFBVOM/RTYY2yLjhmX20P0ytVfs4Ep1h2SM4g9PP8wIDAQAB";

    public static void main(String[] args) throws  Exception{
        checkSign();
        signData();
    }

    private static void checkSign(){
        String raw = "{\"amount\":1,\"payTypes\":\"\",\"signature\":\"fH8o7YkmkUwlGXP4ZqbD4iJfM1pSZNaoGDWHLwdW"
            + "+BofiWMJHMH3GYvzx55sqNeY1m7DZG0LdYUKMg8IK2fI2PU6jK0WbCOurTZZAcT3sKkJC4p31r2I1"
            + "/SKTJIz1T01A2a0z9x98jocjxNVlUpL+uKALZLRx3/T5Y2WG8d6vlKIEVgus6AS8IPKTipU0hEIXPOtrEkqlxrBEU6"
            + "/6YhklALoxrY8DgFq25x2Q3PSFkK8nrnX27eW8BntYnw6uHiLK0JEsxfwntJN+wlIbo1XO4hxUWufvwmOo9oYWzJrPJOE2"
            + "+p3VLXSLMllexlRn73FaYWniOVWCmIUfKNxiZVJGA==\",\"requestId\":\"fd12398811qq2\","
            + "\"payEndTime\":\"20180625163654599\",\"signatureMethod\":\"SHA256WithRSA\","
            + "\"callbackUrl\":\"http://11112222\",\"orderDate\":\"20180625163654599\",\"userId\":\"2333334\","
            + "\"channelId\":\"test_46462896441619506\"}";
		System.out.println(raw);
		boolean result = Signature.verifyPacket(raw, LINKEDMALL_PUBLICK_KEY);
		System.out.println(result);
    }


    public static void  signData() throws  Exception{
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
        keyPairGen.initialize(2048,new SecureRandom());
        KeyPair keyPair = keyPairGen.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        String privateKeyStr = BaseEncoding.base64().encode(privateKey.getEncoded());
        String publicKeyStr = BaseEncoding.base64().encode(keyPair.getPublic().getEncoded());
        System.out.println("公钥：" +publicKeyStr);
        System.out.println("私钥：" +privateKeyStr);
        Map<String, Object> requestMap = buildReqeustMap();
        System.out.println("请求内容:"+JSON.toJSONString(requestMap));
        Map<String, Object> reuslt = Signature.outputJson(requestMap, privateKeyStr);
        String serializedData = JSON.toJSONString(reuslt);
        System.out.println("最终请求报文: "+serializedData);
        boolean v = Signature.verifyPacket(serializedData,publicKeyStr );
        System.out.println("验签结果:"+v);

    }

    private static Map<String, Object> buildReqeustMap(){
         HashMap<String, Object> payType1 = new HashMap<String, Object>();
         payType1.put("tbOrderId", "1233");
         payType1.put("fundAmountMoney", 200);
        HashMap<String, Object> payType2 = new HashMap<String, Object>();
        payType2.put("tbOrderId", "4444");
        payType2.put("fundAmountMoney", 567);
        List<Map<String, Object>> payTypes =new ArrayList<Map<String, Object>>();
        payTypes.add(payType1);
        payTypes.add(payType2);
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("requestId", "mock12345678");
        map.put("orderDate", "20180815101223");
        map.put("payEndTime","20180815121011");
        map.put("amount", 23L);
        map.put("callbackUrl","http://callback.url.com");
        map.put("notifyUrl","http://notify.url.com");
        map.put("userId","u1234");
        map.put("channelId", "tenant12333");
        map.put("payTypes", JSON.toJSONString(payTypes));
        return map;
    }

}
