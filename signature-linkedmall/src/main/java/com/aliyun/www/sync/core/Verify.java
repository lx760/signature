package com.aliyun.www.sync.core;

import com.google.common.io.BaseEncoding;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

/**
 * @author kyle
 * @date 2018/08/17
 */
public class Verify {

    public static boolean verify(String content, String signStr, String signatureMethod, String pubKey){
        return doVerify(content,signStr,signatureMethod, pubKey);
    }

    public static boolean doVerify(String content, String sign, String signatureMethod, String publicKey)
    {
        try
        {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] encodedKey = BaseEncoding.base64().decode(publicKey);
            PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));

			java.security.Signature signature = java.security.Signature
					.getInstance(signatureMethod);

			signature.initVerify(pubKey);
			signature.update(content.getBytes());

            boolean bverify = signature.verify( BaseEncoding.base64().decode(sign) );
            return bverify;

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

    }

}
