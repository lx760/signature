package com.ynet.fudian;

import java.util.Map;

import com.alibaba.fastjson.JSON;

public class Main {

	public static void main(String[] args) {
		//验证
//		verify();
		//例子
		signedAndverify();
	}
	

	public static void verify(){
		// 公钥证书
		String pubKeyPath = "G:/project/fudianbank/mobilePay/cert/enterprise.cet.cer";
//		String singedSrc = "{\"signature\":\"aP+Rj5KQ1/Ddr9Y7pOOOXr8z++EcH3EmpnxfLTWfN4b7sIb05/6lglYht7BhBT2U+cd9tGIyILQ/x90snHoUEBDt09aon1oN0gOAUawuImarTteezzIrmUFZAOg08RYN0dqnSVMSNNfBbRZLTS+zlt1FaKHma+Xxtw90EDsYjQJ4fgQEQ9Xag8HNkQS+kkxgDOdkuRKoXG9BTAOb11tbHVoE+u63KmMySgV3FF/kvuveSaW3wZHr/f9rKby8aAInSEKLsqdz43dppPTX4nfnsS0L/Mihy4cdwzSTHO07x5i85eEpWXc4cxRoAQw9o7qWgoEGRQp5NOho9ViO2Isefw\u003d\u003d\",\"code\":\"0000\",\"message\":\"交易成功\",\"payTypes\":\"\",\"frontPayUrl\":\"https://ebank.richdian.com:9445/mobileChannel/main.html#mobilePay/index/payIndex\",\"signatureMethod\":\"SHA256WithRSA\",\"tenantOrderId\":\"300000003037\"}";
//		String singedSrc = "{\"code\":\"1008\",\"signature\":\"KjYy0fGWBZeltb3QWCiAlMQP8jtLOn3/v6LRgfK/gdPCedASWCuAU4aniG3pm5maNVng4hDfEU0Z9clQZrnuO/81Y6vTx5LcR1zkDexU6fUIz9bquGo6fztV4lhdSiy2Hhqzi4oFXJ5BUBjQQzuGoGNeG7cIKd4NAGE/EMDG4oFH/dS+QvB2wTkIEE9SCQDtL7teKd3/HndADVcukbDieOxTwSPmzNVVO4TOAn/gK/GPSEmGR/gR13NIhp3jSiHcOE7CiVFlZIttcGk5Pr2hYwzfNaSfBrdfIxGw1mr8fuxFdtV5AaDfoYgSjOrJ3o9MqQrIJw9dkpkvyKdrBsQ7Rw==\",\"message\":\"服务内部未明错误\"}";
		String singedSrc = "{\"code\":\"1008\",\"message\":\"服务内部未明错误\",\"signature\":\"KjYy0fGWBZeltb3QWCiAlMQP8jtLOn3/v6LRgfK/gdPCedASWCuAU4aniG3pm5maNVng4hDfEU0Z9clQZrnuO/81Y6vTx5LcR1zkDexU6fUIz9bquGo6fztV4lhdSiy2Hhqzi4oFXJ5BUBjQQzuGoGNeG7cIKd4NAGE/EMDG4oFH/dS+QvB2wTkIEE9SCQDtL7teKd3/HndADVcukbDieOxTwSPmzNVVO4TOAn/gK/GPSEmGR/gR13NIhp3jSiHcOE7CiVFlZIttcGk5Pr2hYwzfNaSfBrdfIxGw1mr8fuxFdtV5AaDfoYgSjOrJ3o9MqQrIJw9dkpkvyKdrBsQ7Rw\u003d\u003d\"}";
		
		System.out.println("singedSrc: " + singedSrc);
		
        Map<String, Object> map = JSON.parseObject(singedSrc);
        String signature = (String)map.get("signature");
        
        System.out.println("signature: " + signature);
        map.remove("signature");
        map.remove("signatureMethod");
        String serializedStr = CertSignUtil.generateSerializeStrFromMap(map);
        
        System.out.println("serializedStr: " + serializedStr);
        
		try {
			
			if(CertSignUtil.verify(serializedStr.getBytes("UTF-8"), signature, pubKeyPath)){
				System.out.println("vefify success!");
			} else {
				System.out.println("vefify failed!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void signedAndverify(){

		// 银行签名证书 SHA1withRSA
//		String certPath = "G:\\project\\coupon\\signature-tianmao\\src\\cert\\3000000640.pfx";// 银行签名证书 SHA1withRSA
		String certPath = "G:/project/fudianbank/mobilePay/cert/enterprise.pfx";
		String keystorePass = "123456";// 银行签名证书密码
		// 公钥证书
//		String pubKeyPath = "G:\\project\\coupon\\signature-tianmao\\src\\cert\\3000000640.cer"; // 公钥证书
		String pubKeyPath = "G:/project/fudianbank/mobilePay/cert/enterprise.cet.cer";

//		String singedSrc = "{\"amount\":\"100.2\",\"callbackUrl\":\"http://www.qiantai.com\",\"channelId\":\"1\",\"notifyUrl\":\"http://www.houtai.com\",\"orderDate\":\"20180627\",\"payEndTime\":\"20180627203212111\",\"requestId\":\"00000000001\",\"userId\":\"0000000000022\"}";
//		String singedSrc = "{\"code\":\"1008\",\"signature\":\"KjYy0fGWBZeltb3QWCiAlMQP8jtLOn3/v6LRgfK/gdPCedASWCuAU4aniG3pm5maNVng4hDfEU0Z9clQZrnuO/81Y6vTx5LcR1zkDexU6fUIz9bquGo6fztV4lhdSiy2Hhqzi4oFXJ5BUBjQQzuGoGNeG7cIKd4NAGE/EMDG4oFH/dS+QvB2wTkIEE9SCQDtL7teKd3/HndADVcukbDieOxTwSPmzNVVO4TOAn/gK/GPSEmGR/gR13NIhp3jSiHcOE7CiVFlZIttcGk5Pr2hYwzfNaSfBrdfIxGw1mr8fuxFdtV5AaDfoYgSjOrJ3o9MqQrIJw9dkpkvyKdrBsQ7Rw==\",\"message\":\"服务内部未明错误\"}";
//		String singedSrc = "{\"code\":\"1008\",\"message\":\"服务内部未明错误\",\"signature\":\"KjYy0fGWBZeltb3QWCiAlMQP8jtLOn3/v6LRgfK/gdPCedASWCuAU4aniG3pm5maNVng4hDfEU0Z9clQZrnuO/81Y6vTx5LcR1zkDexU6fUIz9bquGo6fztV4lhdSiy2Hhqzi4oFXJ5BUBjQQzuGoGNeG7cIKd4NAGE/EMDG4oFH/dS+QvB2wTkIEE9SCQDtL7teKd3/HndADVcukbDieOxTwSPmzNVVO4TOAn/gK/GPSEmGR/gR13NIhp3jSiHcOE7CiVFlZIttcGk5Pr2hYwzfNaSfBrdfIxGw1mr8fuxFdtV5AaDfoYgSjOrJ3o9MqQrIJw9dkpkvyKdrBsQ7Rw\u003d\u003d\"}";
		String singedSrc = "{\"code\":\"1008\",\"message\":\"服务内部未明错误\",\"signature\":\"GkfRobuw55VL9MJArN+tlzKYJFbUGXQdZ+Ku8zroCkIoISq5ojRxgHnZ19AkmEpBRXuVzqKbCfRfS6m0hzpnd6SGEdP8LobdTKngb5Al0vBQpdItmyZfBNP3R8mEJI5cfStFQ9BF4xYSbef/fq/5wJ323qUw4LPcbc74spwGSEmB4CsV9/tDx/S//ua4Dex3ah+acFYzThn862cS5Z+jfpFeMzxJ32sfKiMbpMjJ3LsMvbZy7GoUJzdY0Etpp6rmZlc4no6Z5RLjebfjHfc2tHX3R5N52E86sEnngejMxJqLf+hgPqbwcGZL/JiVINFJ8szFT5g8hndCrUuzioz/hQ\u003d\u003d\"}";
		
		System.out.println("singedSrc: " + singedSrc);
		String signedData = null;
		
        Map<String, Object> map = JSON.parseObject(singedSrc);
        String signature = (String)map.get("signature");
        map.remove("signature");
        map.remove("signatureMethod");
        String serializedStr = CertSignUtil.generateSerializeStrFromMap(map);
        
        System.out.println("serializedStr: " + serializedStr);
		try {
			signedData = CertSignUtil.sign(serializedStr.getBytes("UTF-8"),CertSignUtil.KEY_STORE_PFX,certPath,keystorePass);
			System.out.println("signedData:\n"+signedData);
			
			if(CertSignUtil.verify(serializedStr.getBytes("UTF-8"), signature,pubKeyPath)){
				System.out.println("vefify success!");
			} else {
				System.out.println("vefify failed!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
}
