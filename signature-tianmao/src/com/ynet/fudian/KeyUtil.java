package com.ynet.fudian;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * 这个是key的工具类，用来获得签名私钥和验签公钥
 * 
 * @author zhubilong
 * @date: 2015-12-29
 * 
 */
public class KeyUtil {

	private static KeyReader keyReader;	// 公私钥读取器

	static {
		keyReader = new KeyReader();
	}

	/**
	 * 
	 * 这个类用来得到公钥，返回一个PublicKey
	 * 
	 * @return
	 * @throws Exception
	 */
	public static PublicKey getPubKey(String filename) throws Exception {
		// 注意这里java在读取文件路径中有空格的情况下是会抛异常的。
		PublicKey pubKey = (PublicKey) keyReader.fromCerStoredFile(filename);
		return pubKey;
	}
	
	public static PrivateKey getPriKeyByKeyStoreType(String keyStoreType, String filename, String certpwd) throws Exception {
		PrivateKey priKey = null;
		if("pfx".equalsIgnoreCase(keyStoreType)){
			priKey = keyReader.readPrivateKefromPFXFile(filename, certpwd);
        } else if ("jks".equalsIgnoreCase(keyStoreType)){
        	priKey = keyReader.readPrivateKeyfromJKSFile(filename, certpwd);
        }
		
		return priKey;
	}

	/**
	 * 从PKCS12标准存储格式中读取私钥钥，后缀为.jks文件，该文件中包含私钥 这个类用来得到银行的私钥，返回一个PrivateKey
	 * 
	 * @return
	 * @throws Exception
	 */
	public static PrivateKey getPriKeyFromJKS(String filename, String certpwd) throws Exception {
		PrivateKey priKey = keyReader.readPrivateKeyfromJKSFile(filename, certpwd);
		return priKey;
	}

	/**
	 * 从PKCS12标准存储格式中读取私钥钥，后缀为.pfx文件，该文件中包含私钥 这个类用来得到银行的私钥，返回一个PrivateKey
	 * 
	 * @return
	 * @throws Exception
	 */
	public static PrivateKey getPriKeyFromPFX(String filename, String certpwd) throws Exception {
		PrivateKey priKey = keyReader.readPrivateKefromPFXFile(filename, certpwd);
		return priKey;
	}
	
	/**
	 * 获取证书签名算法
	 * 
	 * @param keyStoreType
	 * @param certFileName
	 * @param certpwd
	 * @return
	 */
	public static String getCertSigAlgName(String keyStoreType, String certFileName, String certpwd){
		return keyReader.getCertSigAlgName(keyStoreType, certFileName, certpwd);
	}
	
	/**
	 * 
	 * 这个方法用来得到公钥，返回一个公钥串
	 */
	public static String getPublicKeyStr(String filename) throws Exception {
		StringBuffer pubKey = new StringBuffer();
		BufferedReader in = null;
		try{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(filename))));
			String line = null;
			while((line = in.readLine()) != null){
				pubKey.append(line);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(in != null){
					in.close();
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return pubKey.toString();
	}
    
}
