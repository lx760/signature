package com.ynet.fudian;

import java.io.FileInputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;

import com.google.common.io.BaseEncoding;

/**
 * 证书签名验签工具类
 * 
 * 功能：从pfx文件读取证书信息，使用私钥签名，公钥验签。
 * 
 * @author zbl
 * @date 2018-07-23
 */
public class CertSignUtil {
	
	// key store类型 pfx 或p12
	public static final String KEY_STORE_PFX = "PFX";

	// key store类型 jks
	public static final String KEY_STORE_JKS = "JKS";
	
	public static final String X509 = "X.509";

	/**
	 * 私钥加密
	 * 
	 * @param data
	 * @param keyStorePath
	 * @param keyStorePassword
	 * @param alias
	 * @param aliasPassword
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPrivateKey(byte[] data, String certFileName, String certpwd) throws Exception {
		// 取得私钥
		PrivateKey privateKey = KeyUtil.getPriKeyFromPFX(certFileName, certpwd);

		// 对数据加密
		Cipher cipher = Cipher.getInstance(privateKey.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);

		return cipher.doFinal(data);
	}

	/**
	 * 私钥解密
	 * 
	 * @param data
	 * @param keyStorePath
	 * @param alias
	 * @param keyStorePassword
	 * @param aliasPassword
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPrivateKey(byte[] data, String certFileName, String certpwd) throws Exception {
		// 取得私钥
		PrivateKey privateKey = KeyUtil.getPriKeyFromPFX(certFileName, certpwd);

		// 对数据加密
		Cipher cipher = Cipher.getInstance(privateKey.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateKey);

		return cipher.doFinal(data);
	}

	/**
	 * 公钥加密
	 * 
	 * @param data
	 * @param certificatePath
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPublicKey(byte[] data, String certFileName) throws Exception {

		// 取得公钥
		PublicKey publicKey = KeyUtil.getPubKey(certFileName);
		// 对数据加密
		Cipher cipher = Cipher.getInstance(publicKey.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);

		return cipher.doFinal(data);
	}

	/**
	 * 公钥解密
	 * 
	 * @param data
	 * @param certificatePath
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPublicKey(byte[] data, String certFileName) throws Exception {
		// 取得公钥
		PublicKey publicKey = KeyUtil.getPubKey(certFileName);

		// 对数据加密
		Cipher cipher = Cipher.getInstance(publicKey.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, publicKey);

		return cipher.doFinal(data);
	}
	
	/**
	 * 获得Certificate
	 * 
	 * @param certificatePath
	 * @return
	 * @throws Exception
	 */
	private static Certificate getCertificate(String certificatePath) throws Exception {
		CertificateFactory certificateFactory = CertificateFactory.getInstance(X509);
		FileInputStream in = new FileInputStream(certificatePath);

		Certificate certificate = certificateFactory.generateCertificate(in);
		in.close();

		return certificate;
	}

	/**
	 * 验证证书是否过期或无效
	 * 
	 * @param date
	 * @param certificate
	 * @return
	 */
	private static boolean verifyCertificate(Date date, Certificate certificate) {
		boolean status = true;
		try {
			X509Certificate x509Certificate = (X509Certificate) certificate;
			x509Certificate.checkValidity(date);
		} catch (Exception e) {
			status = false;
		}
		return status;
	}
	
	/**
	 * 签名
	 * 
	 * @param srcData
	 * @param privateKey
	 * @param CertSigAlgName
	 * @return
	 * @throws Exception
	 */
	public static String sign(byte[] srcData, PrivateKey privateKey, String CertSigAlgName ) throws Exception {
		// 构建签名
		Signature signature = Signature.getInstance(CertSigAlgName);
		signature.initSign(privateKey);
		signature.update(srcData);
		
		return BaseEncoding.base64().encode(signature.sign());
	}

	/**
	 * 签名
	 * 
	 * @param sign
	 * @param certFileName
	 * @param certpwd
	 * @return
	 * @throws Exception
	 */
	public static String sign(byte[] srcData, String keyStoreType, String certFileName, String certpwd) throws Exception {

		// 取得私钥
		PrivateKey privateKey = KeyUtil.getPriKeyByKeyStoreType(keyStoreType,certFileName, certpwd);
		
		String CertSigAlgName = KeyUtil.getCertSigAlgName(keyStoreType,certFileName, certpwd);
		System.out.println("CertSigAlgName:"+CertSigAlgName);
		
		// 构建签名
		Signature signature = Signature.getInstance(CertSigAlgName);
		signature.initSign(privateKey);
		signature.update(srcData);
		
		return (BaseEncoding.base64().encode( signature.sign()));
	}
	
	/**
	 * 公钥证书：验证签名
	 * 
	 * @param srcData
	 * @param signData
	 * @param publicKey
	 * @param CertSigAlgName
	 * @return
	 * @throws Exception
	 */
	public static boolean verify(byte[] srcData,String signData,PublicKey publicKey, String CertSigAlgName ) throws Exception {
		
		// 构建签名
		Signature signature = Signature.getInstance(CertSigAlgName);
		signature.initVerify(publicKey);
		signature.update(srcData);
		
		byte[] sign = BaseEncoding.base64().decode(signData);
		return signature.verify(sign);
	}

	/**
	 * 公钥证书：验证签名
	 * 
	 * @param srcData
	 * @param signData
	 * @param certificatePath
	 * @return
	 * @throws Exception
	 */
	public static boolean verify(byte[] srcData,String signData, String certificatePath) throws Exception {
		
		// 获得证书
		X509Certificate x509Certificate = (X509Certificate) getCertificate(certificatePath);
		// 获得公钥
		PublicKey publicKey = x509Certificate.getPublicKey();

		// 构建签名
		Signature signature = Signature.getInstance(x509Certificate.getSigAlgName());
		signature.initVerify(publicKey);
		signature.update(srcData);
		
		byte[] sign = BaseEncoding.base64().decode(signData);
		return signature.verify(sign);
	}


	public static String generateSerializeStrFromMap(Map<String, Object> map) {
		Map<String, String> serializedValueMap = serializeValueOfMap(map);
		serializedValueMap.remove("signature");
		serializedValueMap.remove("signatureMethod");
		StringBuilder content = new StringBuilder();
		List<String> keys = new ArrayList<String>(serializedValueMap.keySet());
		Collections.sort(keys);
		int index = 0;
		for (int i = 0; i < keys.size(); i++) {
			String key = keys.get(i);
			String value = serializedValueMap.get(key);
//			if (!StringUtils.isEmpty(key) && !StringUtils.isEmpty(value)) {
			
			if(key != null && !key.equals("") && value != null && !value.equals("") ){
				content.append((index == 0 ? "" : "&") + key + "=" + value);
				index++;
			}
		}
		return content.toString();
	}

	private static Map<String, String> serializeValueOfMap(Map<String, Object> map) {
		Map<String, String> result = new HashMap<String, String>(map.size());
		for (Map.Entry<String, Object> entity : map.entrySet()) {
			if (entity.getValue() != null) {
				result.put(entity.getKey(), String.valueOf(entity.getValue()));
			}
		}
		return result;
	}
	
	
}
